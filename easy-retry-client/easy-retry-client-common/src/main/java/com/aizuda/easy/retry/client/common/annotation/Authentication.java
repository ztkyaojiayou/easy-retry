package com.aizuda.easy.retry.client.common.annotation;

/**
 * Easy Retry Client 认证
 *
 * @author: xiaownouniu
 * @date : 2024-03-30
 * @since : 3.2.0
 */
public @interface Authentication {
}
