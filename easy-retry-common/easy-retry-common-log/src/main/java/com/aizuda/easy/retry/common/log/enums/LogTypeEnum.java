package com.aizuda.easy.retry.common.log.enums;

/**
 * @author: xiaowoniu
 * @date : 2024-03-21
 * @since : 3.2.0
 */
public enum LogTypeEnum {

    RETRY,
    JOB
}
